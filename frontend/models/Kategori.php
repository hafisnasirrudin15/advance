<?php

namespace frontend\models;

use Yii;


class Kategori extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kategori';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kategori',], 'required', 'message' => 'ID kategori gakoleh kosong'],
            [['id_kategori'], 'string', 'max' => 5],

            [['kategori',], 'required', 'message' => 'kategori gakoleh kosong'],
            [['kategori'], 'string', 'max' => 20],
            [['id_kategori'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kategori' => 'Id Kategori',
            'kategori' => 'Nama Kategori',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBukus()
    {
        return $this->hasMany(Buku::className(), ['kategori' => 'id_kategori']);
    }
}
