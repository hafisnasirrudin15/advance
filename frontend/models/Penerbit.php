<?php

namespace frontend\models;

use Yii;

class Penerbit  extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'penerbit';
    }


public function rules()
{
    return[
      [['id_penerbit'], 'required', 'message' => 'id tidak boleh kosong'],
      [['id_penerbit'], 'string', 'max' => 7],

      [['nama'], 'required', 'message' => 'id tidak boleh kosong'],
      [['nama'], 'string', 'max' => 30],

      [['alamat'], 'required', 'message' => 'id tidak boleh kosong'],
      [['alamat'], 'string', 'max' => 50],
    ];

}

public function attributeLabels()
{
    return[
        'id_penerbit' => 'ID Penerbit',
        'nama' => 'Nama',
        'alamat' => 'alamat',
    ];
}
}