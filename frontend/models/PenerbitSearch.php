<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Penerbit;

class PenerbitSearch extends Penerbit
{
    public function rules()
    {
        return[
            [['id_penerbit', 'nama', 'alamat'], 'safe'],
        ];
    }
    
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Penerbit::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()){
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'id_penerbit', $this->id_penerbit])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like',  'alamat', $this->alamat]);

            return $dataProvider;
    }

   


}
