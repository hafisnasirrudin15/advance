<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "buku".
 *
 * @property string $id_buku
 * @property string $judul
 * @property string $penerbit
 * @property string $kategori
 *
 * @property Kategori $kategori0
 */
class Buku extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'buku';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_buku'], 'required','message' => 'ojok kosong'],
            [['id_buku'], 'string','max' => 7],

            [['judul'], 'string', 'max' => 50],
            [['judul'], 'required','message' => 'judul gakoleh kosong'],

            [['penerbit'], 'string', 'max' => 30],
            [['penerbit'], 'required','message' => 'penerbit gakoleh kosong'],

            [['kategori'], 'string', 'max' => 20],
            [['kategori'], 'required','message' => 'kategori gakoleh kosong'],

            [['jumlah'], 'integer', 'max' => 10, 'min' => 0, 'message' => 'antara 0 sampai 10'],
            // [['jumlah'], 'required','message' => 'kategori gakoleh kosong'],

            [['id_buku'], 'unique'],
            [['kategori'], 'exist', 'skipOnError' => true, 'targetClass' => Kategori::className(), 'targetAttribute' => ['kategori' => 'id_kategori']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_buku' => 'Id Buku',
            'judul' => 'Judul',
            'penerbit' => 'Penerbit',
            'kategori' => 'ID Kategori',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKategori0()
    {
        return $this->hasOne(Kategori::className(), ['id_kategori' => 'kategori']);
    }
}
