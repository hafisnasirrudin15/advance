<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Buku */



$this->title = 'Fitur Tambah Buku v.1.0.0';
$this->params['breadcrumbs'][] = ['label' => 'Bukus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buku-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
