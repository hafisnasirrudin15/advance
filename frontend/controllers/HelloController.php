<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;


class HelloController extends Controller
{

 public function actions()
    {
        return [
            'error' => ['class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        echo "Hello World";
    }

   public function actionShow($x='null')
   {
   		if($x==='null'){
   			echo "anda belum memasukkan variabel nama";
   		}else{
   			echo "Hallo". $x ;
   		}
   			
   }
    
}